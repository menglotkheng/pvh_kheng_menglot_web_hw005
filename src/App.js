import ContentComponent from "./components/ContentComponent";
import MyRightComponent from "./components/MyRightComponent";
import SideBarComponent from "./components/SideBarComponent";

function App() {
  return (
    <div className="grid grid-cols-12 h-screen">
      <div className="col-span-1">
        <SideBarComponent />
      </div>
      <div className="col-span-7">
        <ContentComponent />
      </div>
      <div className="col-span-4">
        <MyRightComponent />
       
      </div>
    </div>
  );
}

export default App;
