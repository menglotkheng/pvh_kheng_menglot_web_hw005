import React from "react";
import category_icon from "../assets/category_icon.png";
import cube from "../assets/cube.png";
import list from "../assets/list.png";
import messenger from "../assets/messenger.png";
import sucessful from "../assets/successful.png";
import security from "../assets/security.png";
import users from "../assets/users.png";
import christina from "../assets/christina.jpg";
import lachlan from "../assets/lachlan.jpg";
import raamin from "../assets/raamin.jpg";
import plus from "../assets/plus.png";

const SideBarComponent = () => {
  return (
    <div>
      <aside
        id="default-sidebar"
        class="fixed top-0 left-0 z-40 h-screen transition-transform -translate-x-full sm:translate-x-0"
        aria-label="Sidebar"
      >
        <div class="h-full   px-3 py-4 overflow-y-auto bg-gray-50 dark:bg-gray-800">
          <ul class="space-y-2">
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={category_icon} className="w-6"></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={cube} className="w-6"></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={list} className="w-6"></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={messenger} className="w-6"></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={list} className="w-6"></img>
              </a>
            </li>
          </ul>

          <ul class="space-y-2 mt-10">
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={sucessful} className="w-6"></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={security} className="w-6"></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={users} className="w-6"></img>
              </a>
            </li>
          </ul>

          <ul class="space-y-2 mt-10">
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img
                  src={christina}
                  className=" rounded-full w-[32px] h-[32px] "
                ></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img
                  src={lachlan}
                  className=" rounded-full w-[32px] h-[32px] "
                ></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class=" flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img
                  src={raamin}
                  className="w-[32px] h-[32px] rounded-full"
                ></img>
              </a>
            </li>
            <li>
              <a
                href="#"
                class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700"
              >
                <img src={plus} className="w-8 "></img>
              </a>
            </li>
          </ul>
        </div>
      </aside>
    </div>
  );
};

export default SideBarComponent;
