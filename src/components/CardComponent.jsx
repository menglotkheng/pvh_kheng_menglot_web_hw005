import React from "react";
import { useState } from "react";

const CardComponent = ({ dataTrip, setTrip }) => {
  // change button status
  const [more, setMore] = useState([]);
  const changeButton = (i) => {
    if (dataTrip[i].type === "camping") {
      dataTrip[i].type = "archery";
    } else if (dataTrip[i].type === "archerytemple") {
      dataTrip[i].type = "hil walk";
    } else {
      dataTrip[i].type = "camping";
    }
    setTrip([...dataTrip]);
  };

  
  return (
    <div>
      {/* card box */}
      <div class="flex flex-wrap gap-5 mb-10">
        {dataTrip.map((t, i) => (
          <div class="max-w-sm p-6 w-[500px] h-[280px] bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <a href="#">
              <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                {t.title}
              </h5>
            </a>
            <p class="mb-3 font-normal text-gray-700 dark:text-gray-400 line-clamp-3">
              {t.description}
            </p>
            <div class="mt-3 ">
              <h1>People Going</h1>
              <h1 class="font-bold text-lg">{t.total}</h1>
            </div>
            <button
              type="button"
              class={
                t.type === "camping"
                  ? "py-3.5 px-6 mr-2 mt-6 text-sm font-medium text-gray-900 focus:outline-none bg-pink-500 rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 "
                  : t.type === "archery"
                  ? "py-3.5 px-6 mr-2 mt-6 text-sm font-medium text-gray-900 focus:outline-none bg-yellow-500 rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 "
                  : "py-3.5 px-6 mr-2 mt-6 text-sm font-medium text-gray-900 focus:outline-none bg-green-500 rounded-lg border border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200 "
              }
              onClick={() => changeButton(i)}
            >
              {t.type}
            </button>
            {/* The button to open modal */}
            <label
              htmlFor="my-modal-3"
              className="btn"
              onClick={() => setMore(t)}
            >
              Read More
            </label>

            {/* Put this part before </body> tag */}
            <input type="checkbox" id="my-modal-3" className="modal-toggle" />
            <div className="modal">
              <div className="modal-box relative">
                <label
                  htmlFor="my-modal-3"
                  className="btn btn-sm btn-circle absolute right-2 top-2"
                >
                  ✕
                </label>
                <h3 className="text-lg font-bold" id="title">
                  {more.title}
                </h3>
                <p className="py-4" id="dc">
                  {more.description}
                </p>
                <p>
                  Around{" "}
                  <span class="font-bold" id="total">
                    {more.total}
                  </span>{" "}
                  People going there
                </p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CardComponent;
