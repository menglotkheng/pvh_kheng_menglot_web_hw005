import React from "react";
import { useState } from "react";
import CardComponent from "./CardComponent";
import FormComponent from "./FormComponent";
const ContentComponent = () => {
  const [model, setModel] = useState(false);

  // declare state

  const [trip, setTrip] = useState([
    {
      id: 1,
      title: "Kirirom",
      description:
        "The word Kirirom in Khmer translates to Happy Mountain.  Prasat Nean Khmao hosts few visitors: a welcome change from the Siem Reap temples.",
      type: "mountain",
      total: "19807",
    },
    {
      id: 2,
      title: "Prasat Neang Khmau",
      description:
        " Prasat Nean Khmao hosts few visitors: a welcome change from the Siem Reap temples. Prasat Nean Khmao hosts few visitors: a welcome change from the Siem Reap temples.",
      type: "temples",
      total: "78960",
    },
    {
      id: 3,
      title: "Prasat Neang Khmau",
      description:
        " Prasat Nean Khmao hosts few visitors: a welcome change from the Siem Reap temples. Prasat Nean Khmao hosts few visitors: a welcome change from the Siem Reap temples.",
      type: "popo",
      total: "78960",
    },
  ]);

  return (
    <div>
      <div class="flex justify-between mt-10">
        <div>
          <h1 class="font-bold text-lg">Good Morning Team Work</h1>
        </div>
        <div>
          <button
            onClick={() => {
              setModel(true);
            }}
            type="button"
            class="text-white  bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          >
            Add New Trip
          </button>
        </div>
      </div>

      {/* calling form  */}

      <FormComponent
        result={model}
        oncls={() => {
          setModel(false);
        }}
        trips={trip}
        setTrips={setTrip}
      ></FormComponent>

      {/* //calling CardComponent */}
      <CardComponent dataTrip={trip} setTrip={setTrip}></CardComponent>
    </div>
  );
};

export default ContentComponent;
