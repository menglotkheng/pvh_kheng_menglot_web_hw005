import React from "react";
import christina from "../assets/christina.jpg";
import notification from "../assets/notification.png";
import comment from "../assets/comment.png";
import lachlan from "../assets/lachlan.jpg";
import raamin from "../assets/raamin.jpg";

const MyRightComponent = () => {
  return (
    <div>
      <div class="h-screen bg-profile bg-cover bg-center ">
        <ul class="space-y-2 inline-flex ml-[65%]">
          <li class="mt-2">
            <a
              href="#"
              class="flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white  dark:hover:bg-gray-700"
            >
              <img src={notification} className="  w-[32px] h-[32px] "></img>
            </a>
          </li>
          <li>
            <a
              href="#"
              class=" flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white dark:hover:bg-gray-700"
            >
              <img src={comment} className="w-[32px] h-[32px] "></img>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white  dark:hover:bg-gray-700"
            >
              <img
                src={christina}
                className="w-[32px] h-[32px] rounded-full "
              ></img>
            </a>
          </li>
        </ul>
        {/* button */}
        <button
          type="button"
          class=" mt-7 ml-[65%] focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-6 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
        >
          My Amazing Trips
        </button>

        {/* text */}
        <p class="text-white text-lg indent-10 font-bold ml-5 mt-5">
          Save traveler-recommended places for your trip · View the things to
          do, restaurants and hotels you saved on a map · Easily access all your
          saves while traveling
        </p>
        {/* profile */}
        <p class="text-white text-lg indent-5 font-bold mt-10">
          27 People going to this trip
        </p>
        <ul class="space-y-2 inline-flex ml-5">
          <li class="mt-2">
            <a
              href="#"
              class="flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white hover:bg-pink-400 dark:hover:bg-gray-700"
            >
              <img
                src={christina}
                className=" rounded-full w-[32px] h-[32px] "
              ></img>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white hover:bg-pink-400 dark:hover:bg-gray-700"
            >
              <img
                src={lachlan}
                className=" rounded-full w-[32px] h-[32px] "
              ></img>
            </a>
          </li>
          <li>
            <a
              href="#"
              class=" flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white hover:bg-pink-400 dark:hover:bg-gray-700"
            >
              <img
                src={raamin}
                className="w-[32px] h-[32px] rounded-full"
              ></img>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex items-center p-2 text-base font-normal text-gray-900 rounded-full dark:text-white hover:bg-pink-400 dark:hover:bg-gray-700"
            >
              <img
                src={lachlan}
                className=" rounded-full w-[32px] h-[32px] "
              ></img>
            </a>
          </li>
          <li>
            <a
              href="#"
              class="flex items-center mt-1 ml-2 p-2 font-bold  w-[40px] h-[40px] text-base  text-gray-900 rounded-full hover:bg-pink-400 dark:text-white bg-pink-300 dark:hover:bg-gray-700"
            >
              99+
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default MyRightComponent;
